#!/usr/bin/env python

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='multigeo',
    description='Multiple types of reverse geocoding',
    url='https://gitlab.com/danjones000/multigeo',
    version='0.0.11',
    python_requires='>3.4.0',
    author='Dan Jones',
    author_email='danjones@goodevilgenius.org',
    license='MIT',
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'multigeo=multigeo.multigeo:main',
            'multigeo-list=multigeo.multigeo:list_services',
            'multigeo-places=multigeo.gplaces:main',
            'multigeo-4sq=multigeo.foursquare:main'
        ]
    },
    package_data={
        'multigeo': ['scripts/*', 'tasks/*']
    },
    include_package_data=True,
    project_urls={
        "Bug Tracker": "https://gitlab.com/danieljones000/multigeo/issues",
        "Documentation": "https://gitlab.com/danieljones000/multigeo/blob/stable/README.md",
        "Source Code": "https://gitlab.com/danieljones000/multigeo"
    },
    install_requires=[
        "argparse",
        "appdirs",
        "requests"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)
