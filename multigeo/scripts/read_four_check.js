/* global stdout */

let out = JSON.parse(stdout);

var http_code = out.meta.code || 600;
var venue_id = '';
var created_at = '';
var venue_name = '';
var foursquare_failure = '';

if (http_code == 200) {
    venue_id = out.venue.id;
    venue_name = out.venue.name;
    created_at = out.createdAt;
} else {
    foursquare_failure = '' + out.meta.code + ': ' + (out.meta.errorDetail || 'Unknown error');
}
