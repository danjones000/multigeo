/** global post, manual, note */

let p = JSON.parse(post);

if (typeof manual == 'string') {
    let m = JSON.parse(manual);

    if (!p.id) {
        p = m;
    } else {
        p.address = p.title;
        p.title = m.title;
    }

}

if (typeof note == 'string') {
    p.note = note;
}

var post = JSON.stringify(p);
