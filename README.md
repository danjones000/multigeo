# multigeo

The purpose of this project is to get geographical information about your current location to use in Tasker. How you choose to use it in Tasker is up to you.

To use this tool, you need a few things.

## Prerequisites

- [Tasker](https://tasker.joaoapps.com/)
- [AutoTools](https://joaoapps.com/autotools-supercharge-tasker/)
- [Termux](https://termux.com/)
- [Termux:Tasker](https://wiki.termux.com/wiki/Termux:Tasker)
- Python should be installed within Termux: `pkg install python`
- Storage should be setup within Termux: `termux-setup-storage`

## Installation

### Within Termux

Running the following commands will install `multigeo` within Termux, and set most of it up to be used with Tasker:

```bash
python -m pip install --upgrade git+https://gitlab.com/danjones000/multigeo.git@stable
python -m multigeo.prep_tasker
```

Alternately, if you've cloned the repo, from within the root of the repo directory, run:

```bash
./bin/update.sh
```

To update, follow the same instructions. If using the local repo, the update script does a `git pull` first, so you'll always get the most recent version.

#### Setting up API keys

*Still to be documented*

### Within Tasker

The previous step copied a `.tsk.xml` file to `Tasker/tasks` folder within your main Android folder.

Now, open up Tasker, and long press on the "Tasks" tab. Click "Import Task". Navigate to the `Tasker/tasks` folder, and choose `Get_Place.tsk.xml`.

Finally, use your imagination as to what to do next. Figure out how you want to run that task. I recommend launching the task from within another task. Once the task is run, it will store the chosen location into a JSON object. That string will be stored in the `%GEO` global variable, and also returned from the task. So, you can use it from within another task, and store the value to a local variable.

#### Checking in to Foursquare

Similar to the `Get_Place.tsk.xml` task, add the `Check_In_FourSquare.tsk.xml` task.

This task must be launched from another task. I recommend creating a task that calls both of them. It should do something like this:

1. Perform Task: Get Place
    - Return Value Variable: %post
2. *Do some other things*
3. Javascriptlet

    ```javascript
    let p = JSON.parse(post);
    var type = p.type;
    if (type == 'foursquare') {
      var four_id = p.id;
      var shout = p.note;
    }
    ```
4. Perform Task: Check In FourSquare
    - Parameter 1: %four_id
    - Parameter 2: %shout
    - If %type = foursquare

## Future improvements

- More geolocation services
- Better documentation
    + Including screenshots explaining what all this does
    + Also, explain how to get the API keys you'll need
- The ability to choose which services to use
- Add to PyPi, so you can install with `pip install multigeo`
