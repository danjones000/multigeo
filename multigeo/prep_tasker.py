import sys
import os.path
import os
import glob
import shutil


def check_system():
    if sys.platform != 'linux':
        print('Must be run from Linux', file=sys.stderr)
        sys.exit(1)
    first_path = [p for p in sys.path if bool(p)][0]
    if first_path.find('termux') == -1:
        print('Must be run from Termux', file=sys.stderr)
        sys.exit(1)


def get_rel_dir(path):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), path)


def get_tasker_dir():
    storage_dir = os.path.expanduser('~/storage/shared')
    if not os.path.exists(storage_dir):
        print('%s is missing. Please run termux-setup-storage' % storage_dir, file=sys.stderr)
        sys.exit(1)
    tasker_dir = os.path.join(storage_dir, 'Tasker')
    if not os.path.exists(tasker_dir):
        os.mkdir(tasker_dir)
    return tasker_dir


def copy_files(path):
    src = get_rel_dir(path)
    tasker_dir = get_tasker_dir()
    dest = os.path.join(tasker_dir, path)
    if not os.path.exists(dest):
        os.mkdir(dest)
    for f in glob.glob(os.path.join(src, '*')):
        final = shutil.copy(f, dest)
        print("Copied %s to %s" % (f, final))


def setup_symlinks():
    dest_dir = os.path.expanduser('~/.termux/tasker')
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    execs = ['multigeo', 'multigeo-list', 'multigeo-places', 'multigeo-4sq']
    for prog in execs:
        dest = os.path.join(dest_dir, prog)
        if os.path.exists(dest):
            continue
        current = shutil.which(prog)
        os.symlink(current, dest)
        print("%s symlinked to %s" % (current, dest))


def main():
    check_system()
    copy_files('scripts')
    copy_files('tasks')
    setup_symlinks()


if __name__ == "__main__":
    main()
