from .multigeo import get_google_key, clean_results
import argparse
import json
import requests


def info(id, original=False):
    key = get_google_key()
    url = "https://maps.googleapis.com/maps/api/place/details/json"
    query = {"key": key, "place_id": id}
    resp = requests.get(url=url, params=query)
    res = resp.json()['result']

    if original:
        return res

    return {
        "type": "google",
        "id": id,
        "title": res['name'] if "name" in res else res['formatted_address'],
        "location": (
                str(res['geometry']['location']['lat']) + ',' + str(res['geometry']['location']['lng'])
            ) if 'geometry' in res and 'location' in res['geometry'] else None,
        "address": res['formatted_address'],
        "url": res['website'] if "website" in res else res['url'],
        "phone": res['formatted_phone_number'] if "formatted_phone_number" in res else res['international_phone_number'] if "international_phone_number" in res else None,
        "vicinity": res['vicinity'] if "vicinity" in res else None
    }


def get_args():
    p = argparse.ArgumentParser()
    p.add_argument(
        '--original', '-o',
        help="Return original results from API (implies unencoded)",
        action="store_true"
    )
    subs = p.add_subparsers(dest='sub', help='Command')
    info = subs.add_parser('info', help='Get Place info')
    info.add_argument('id', help='The place_id from Google')

    return p.parse_args()


def main():
    args = get_args()
    res = None
    if args.sub == 'info':
        res = clean_results(info(args.id, args.original))
    print(json.dumps(res))


if __name__ == "__main__":
    main()
