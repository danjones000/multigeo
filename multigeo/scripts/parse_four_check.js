/* global stdout */

let out = JSON.parse(stdout);

var http_code = out.meta.code || 600;
var total_points = 0;
var points_message = '';

if (http_code == 200) {
    total_points = out.score.total;
    points_message = out.score.scores.reduce((carry, point) => '\n' + point.points + ': ' + point.message, '').trim();
} else {
    points_message = '' + out.meta.code + ': ' + (out.meta.errorDetail || 'Unknown error');
}
