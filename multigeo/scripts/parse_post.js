/* global post, manual */

function parseStr(str) {
    str = str.trim();
    if (!str || str[0] == '%') return undefined;
    else if (str == 'null') return null;
    return str;
}

function parseArray(arr) {
    arr = arr.map(function(v) {
        if (v === null) return v;
        else if (Array.isArray(v)) v = parseArray(v);
        else if (typeof v == 'object') v = fix(v);
        else if (typeof v == 'string') v = parseStr(v);

        return v;
    }).filter(i => typeof i != 'undefined' && i !== null);

    return arr.length ? arr : undefined;
}

function fix(obj) {
    for (let k in obj) {
        let v = obj[k];

        if (v === null) continue;
        else if (Array.isArray(v)) v = parseArray(v);
        else if (typeof v == 'object') v = fix(v);
        else if (typeof v == 'string') v = parseStr(v);
        else continue;

        obj[k] = v;
    }

    return obj;
}

let p = JSON.parse(post);
p = fix(p);
post = JSON.stringify(p);

if (typeof manual != 'undefined') {
    let m = JSON.parse(manual);
    m = fix(m);
    manual = JSON.stringify(m);
}

// If it's a google location, we can fetch more information about it.
var type = '';
var place_id = '';

type = p.type;
place_id = p.id;
