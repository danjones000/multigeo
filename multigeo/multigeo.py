import os.path
import argparse
import appdirs
from enum import Enum
import requests
import json
import base64


class Service(Enum):
    foursquare = 'Foursquare'
    gplaces = 'Google Places'
    geolocate = 'Google Reverse Geocoding'

    @classmethod
    def from_name(cls, name):
        return cls[name]

    def __str__(self):
        return self.name


def get_api_key_path(service):
    config_dir = appdirs.user_data_dir('multigeo', 'DanielRayJones')
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)
    return os.path.join(config_dir, service) + ".json"


def get_api_key(service):
    path = get_api_key_path(service)
    key = None
    with open(path) as fp:
        key = json.load(fp)

    return key


def get_google_key():
    return get_api_key('google')['key']


def get_foursquare_key():
    key = get_api_key("foursquare")
    if "oauth_token" in key and bool(key['oauth_token']):
        return {"oauth_token": key['oauth_token']}

    if "redirect_uri" not in key or not key['redirect_uri']:
        return {"client_id": key['client_id'], "client_secret": key['client_secret']}

    auth_url = "https://foursquare.com/oauth2/authenticate"
    auth_query = {
        "client_id": key['client_id'],
        "response_type": "code",
        "redirect_uri": key['redirect_uri']
    }
    full_auth_url = requests.Request('GET', auth_url, params=auth_query).prepare().url
    code = input("To authorize with foursquare, go to %s and paste code provided after login: " % full_auth_url)

    token_request_url = "https://foursquare.com/oauth2/access_token"
    query = {
        "client_id": key['client_id'],
        "client_secret": key['client_secret'],
        "grant_type": "authorization_code",
        "redirect_uri": key['redirect_uri'],
        "code": code
    }
    resp = requests.get(url=token_request_url, params=query)
    data = resp.json()
    token = data['access_token']
    key['oauth_token'] = token
    with open(get_api_key_path("foursquare"), 'w') as f:
        json.dump(key, f)

    return {"oauth_token": token}


def foursquare(latlng, original=False):
    key = get_foursquare_key()
    url = "https://api.foursquare.com/v2/venues/search"
    query = key.copy()
    query['v'] = '20200529'
    query['ll'] = latlng
    query['limit'] = 25
    resp = requests.get(url=url, params=query)
    data = resp.json()
    if original:
        return data

    return [
        {
            "type": "foursquare",
            "id": res['id'],
            "title": res['name'],
            "url": "https://foursquare.com/v/%s" % res['id'],
            "count": res['counts'] if 'counts' in res else None,
            "location": (
                str(res['location']['lat']) + ',' + str(res['location']['lng'])
            ) if 'location' in res and 'lat' in res['location'] else None,
            "address": "\n".join(res['location']['formattedAddress']) if 'location' in res and 'formattedAddress' in res['location'] else None
        } for res in data['response']['venues']
    ]


def gplaces(latlng, original=False):
    key = get_google_key()
    query = {"key": key, "location": latlng, "rankby": "distance"}
    url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    resp = requests.get(url=url, params=query)
    data = resp.json()

    if original:
        return data

    return [
        {
            "type": "google",
            "id": res['place_id'],
            "title": res['name'],
            "location": (
                str(res['geometry']['location']['lat']) + ',' + str(res['geometry']['location']['lng'])
            ) if 'geometry' in res and 'location' in res['geometry'] else None
        } for res in data['results']
    ]


def geolocate(latlng, original=False):
    key = get_google_key()
    query = {"key": key, "latlng": latlng}
    url = "https://maps.googleapis.com/maps/api/geocode/json"
    resp = requests.get(url=url, params=query)
    data = resp.json()

    if original:
        return data

    return [
        {
            "type": "google",
            "id": res['place_id'],
            "title": res['formatted_address'],
            "location": (
                str(res['geometry']['location']['lat']) + ',' + str(res['geometry']['location']['lng'])
            ) if 'geometry' in res and 'location' in res['geometry'] else None
        } for res in data['results']
    ]


def clean_results(result):
    if result is None:
        return None
    return {k: v for k, v in result.items() if v is not None}

def encode_results(results):
    if results is None:
        return {"keys": [], "values": []}

    return {
        "keys": [base64.b64encode(bytes(json.dumps(result), 'utf-8')).decode() for result in results],
        "values": [result['title'].replace(',', '.') for result in results]
    }


def list_services():
    p = argparse.ArgumentParser()
    p.add_argument('--manual', '-m', help='Add "manual" to the list', action="store_true")
    args = p.parse_args()

    services = {s.name: s.value for s in list(Service)}
    if args.manual:
        services['manual'] = 'Enter Manually'
    print(json.dumps(services))


def get_args():
    p = argparse.ArgumentParser()
    p.add_argument(
        "service",
        type=Service.from_name,
        choices=list(Service),
        help="The name of the service to use"
    )
    p.add_argument("latlng", help="The latitude and longitude")
    p.add_argument(
        '--unencoded', '-u',
        help="Return unencoded results",
        action="store_true"
    )
    p.add_argument(
        '--original', '-o',
        help="Return original results from API (implies unencoded)",
        action="store_true"
    )

    return p.parse_args()


def main():
    args = get_args()

    func = foursquare if args.service == Service.foursquare else (
        gplaces if args.service == Service.gplaces else (
            geolocate if args.service == Service.geolocate else None
        )
    )
    results = func(args.latlng, args.original) if func is not None else []
    cleaned = [clean_results(res) for res in results]

    if not args.unencoded:
        cleaned = encode_results(cleaned)

    print(json.dumps(cleaned))


if __name__ == "__main__":
    main()
