from .multigeo import get_foursquare_key
import argparse
import json
import requests


def me():
    key = get_foursquare_key()
    url = "https://api.foursquare.com/v2/users/self"
    query = key.copy()
    query['v'] = '20200529'
    resp = requests.get(url=url, params=query)
    ret = resp.json()
    if ret['meta']['code'] == 200:
        meta = ret['meta']
        ret = ret['response']['user']
        ret['meta'] = meta

    return ret


def last_check():
    key = get_foursquare_key()
    url = "https://api.foursquare.com/v2/users/self/checkins"
    query = key.copy()
    query['v'] = '20200529'
    query['limit'] = 1
    resp = requests.get(url=url, params=query)
    ret = resp.json()
    if ret['meta']['code'] == 200:
        meta = ret['meta']
        ret = ret['response']['checkins']['items'][0]
        ret['meta'] = meta

    return ret


def check_in(args):
    key = get_foursquare_key()
    url = "https://api.foursquare.com/v2/checkins/add"
    data = {"venueId": args.id, "v": '20200529'}
    if args.shout is not None:
        data['shout'] = args.shout
    if args.location is not None:
        data['ll'] = args.location
    resp = requests.post(url=url, params=key, data=data)
    ret = resp.json()
    if ret['meta']['code'] == 200:
        meta = ret['meta']
        ret = ret['response']['checkin']
        ret['meta'] = meta

    return ret


def get_args():
    p = argparse.ArgumentParser()
    subs = p.add_subparsers(dest='sub', help='Command')
    me = subs.add_parser('me', help="Get info about current user")
    last_check = subs.add_parser('last-check', help="Get latest checkin")
    check_in = subs.add_parser('check-in', help="Check in to venue")
    check_in.add_argument('id', help="Foursquare ID of the Venue")
    check_in.add_argument('--shout', '-s', help="Shout to accompany check-in", required=False)
    check_in.add_argument('--location', '-l', help="Latitude and Longitude of user", required=False)

    return p.parse_args()


def main():
    args = get_args()
    res = None
    if args.sub == 'me':
        res = me()
    elif args.sub == 'last-check':
        res = last_check()
    elif args.sub == 'check-in':
        res = check_in(args)
    print(json.dumps(res))


if __name__ == "__main__":
    main()
